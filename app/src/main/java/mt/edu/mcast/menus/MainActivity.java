package mt.edu.mcast.menus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = findViewById(R.id.imageView);
        registerForContextMenu(imageView);

        Button buttonPop = findViewById(R.id.btnPop);
        buttonPop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(v);
            }
        });
    }


    private void showToast(CharSequence msg, int duration){
        Toast toast = Toast.makeText(getApplicationContext(), msg, duration);
        toast.show();
    }


    //Options Menu
    public boolean onCreateOptionsMenu(Menu menu){

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_options, menu);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){

            case R.id.sitm_one:
                showToast("SubMenu 1", Toast.LENGTH_SHORT);
                break;
            case R.id.sitm_Two:
                showToast("SubMenu 2", Toast.LENGTH_SHORT);
                break;
            case R.id.itm_two:
                showToast("item 2", Toast.LENGTH_SHORT);
                break;
            case R.id.itm_three:
                showToast("item 3", Toast.LENGTH_SHORT);
                break;
            default: return false;

        }
        return true;

    }

    //Context Menu
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo info){

        super.onCreateContextMenu(menu, v, info);
        MenuInflater inflater = getMenuInflater();
        menu.setHeaderTitle("Context Menu");
        menu.setHeaderIcon(R.drawable.baseline_build_black_18dp);
        inflater.inflate(R.menu.menu_context, menu);

    }

    public boolean onContextItemSelected(MenuItem item){

        switch(item.getItemId()){

            case R.id.citm1:
                showToast("Context 1", Toast.LENGTH_SHORT);
                break;
            case R.id.citm2:
                showToast("Context 2", Toast.LENGTH_SHORT);
                break;
            default: return false;

        }
        return true;

    }

    //popup menu

    public void showPopup(View v){
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()){

                    case R.id.pitm1:
                        showToast("popup 1", Toast.LENGTH_SHORT);
                        break;
                    case R.id.pitm2:
                        showToast("popup 2", Toast.LENGTH_SHORT);
                        break;
                    default: return false;

                }
                return true;
            }
        });

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_popup, popupMenu.getMenu());
        popupMenu.show();
    }

}
